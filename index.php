<?php

/**
 * Created by PhpStorm.
 * User: freakyaxel
 * Date: 17/07/16
 */

require "vendor/autoload.php";

/* DEBUG */
$DEBUG_ENABLED = true;

$configuration = [];
if($DEBUG_ENABLED) {
    $configuration = [
        'settings' => [
            'displayErrorDetails' => true,
        ]
    ];
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

$c = new \Slim\Container($configuration);

$app = new \Slim\App($c);

require "src/routes.php";
require "src/routes-error.php";

$app->run();
