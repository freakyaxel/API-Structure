<?php

/**
 * Created by PhpStorm.
 * User: freakyaxel
 * Date: 17/07/16
 */

return [
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'port'      => '8888',
    'database'  => 'api_test',
    'username'  => 'root',
    'password'  => 'root',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci'
];
