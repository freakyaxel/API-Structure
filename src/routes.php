<?php

/**
 * Created by PhpStorm.
 * User: freakyaxel
 * Date: 17/07/16
 */

// Add documentation route
$app->get('/',
    function () use ($app) {
        echo file_get_contents("doc/index.html");
    }
);

$cp = "Api\\Controllers\\";

$app->get('/local',                        $cp."MainController:local");
$app->post('/database',                    $cp."MainController:database");
$app->get('/parameter/{name}',             $cp."MainController:parameter");