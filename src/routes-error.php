<?php

/**
 * Created by PhpStorm.
 * User: freakyaxel
 * Date: 17/07/16
 */

/* Check this for more details:
 * http://www.slimframework.com/docs/handlers/error.html
 */

$c = $app->getContainer();

$c['notFoundHandler'] = function ($c) {
    return function () use ($c) {
        $resp = 	[
            "success" 	=> 	false,
            "details"   =>  "URL is not a valid service. Check API documentation or contact 'freakyaxel@gmail.com' for help."
        ];
        return $c['response']->withStatus(404)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(
                $resp,
                JSON_PRETTY_PRINT |
                JSON_UNESCAPED_UNICODE |
                JSON_UNESCAPED_SLASHES
            ));
    };
};
