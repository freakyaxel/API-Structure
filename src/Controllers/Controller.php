<?php
/**
 * Created by PhpStorm.
 * User: freakyaxel
 * Date: 17/07/16
 */

namespace Api\Controllers;

use \Slim\Container as SlimContainer;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

abstract class Controller {

    private $_database;

    protected $response;
    protected $request;
    protected $params;
    protected $body;

    /**
     * Controller constructor.
     * @param SlimContainer $cont
     */
    public function __construct(SlimContainer $cont) {
        //Creates controller
        $this->request = $cont->request;
        $this->response = $cont->response;
        $this->params = $this->request->getQueryParams();
        $this->body = $this->request->getParsedBody();

        //Create database connection
        $this->db = new Capsule;
        $this->_database = require __DIR__ . '/../../config/database.php';
        $this->db->addConnection($this->_database);

        // Set the event dispatcher used by Eloquent models... (optional)
        $this->db->setEventDispatcher(new Dispatcher(new Container));

        // Make this Capsule instance available globally via static methods... (optional)
        $this->db->setAsGlobal();

        // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
        $this->db->bootEloquent();
    }

    /**
     * @param array $results
     * @param string $message
     * @return \Psr\Http\Message\ResponseInterface|static
     */
    protected function sendError($results = null, $message = "Request error") {

        if (!is_array($results) && !is_object($results)) $results = [];

        $resp = 	[
            "success" 	=> 	false,
            "details"   =>  $message,
            "results" 	=> 	$results
        ];

        $this->response = $this->response->withStatus(400)->withAddedHeader('Content-Type', 'application/json; charset=utf-8');
        $body = $this->response->getBody();
        $body->write($this->getJSON($resp));

        return $this->response;
    }

    /**
     * @param array $results
     * @param string $message
     * @return \Psr\Http\Message\ResponseInterface|static
     */
    protected function sendSuccess($results = null, $message = "Request successful") {

        if (!is_array($results) && !is_object($results)) $results = [];

        $resp = 	[
            "success" 	=> 	true,
            "details"   =>  $message,
            "results" 	=> 	$results
        ];

        $this->response = $this->response->withStatus(200)->withAddedHeader('Content-Type', 'application/json; charset=utf-8');
        $body = $this->response->getBody();
        $body->write($this->getJSON($resp));

        return $this->response;
    }

    /**
     * @param \Exception $e
     * @return Controller|\Psr\Http\Message\ResponseInterface
     */
    protected function sendException(\Exception $e) {
        if(!empty($e->getMessage())) return $this->sendError(null, $e->getMessage());
        else return $this->sendError(null, $e->getTrace());
    }

    /**
     * Generate random string
     * @author freakyaxel
     * @param  integer [$length = 10] String length
     * @return string String
     */
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @param $string
     * @return string
     */
    public function getJSON($string) {
        return json_encode($string, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}
