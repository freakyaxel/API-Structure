<?php
/**
 * Created by PhpStorm.
 * User: freakyaxel
 * Date: 17/07/16
 */

namespace Api\Controllers;



use Api\Models\TestModel;

class MainController extends Controller {

    public function __construct($cont) {
        parent::__construct($cont);
    }

    public function local() {
        try {
            if(!isset($this->params["name"])) {
                throw new \Exception("'name' parameter is required!");
            }
            $name = $this->params["name"];
            return $this->sendSuccess(null, "Hello $name!");
        } catch (\Exception $e) {
            return $this->sendException($e);
        }
    }

    public function database() {
        try {
            if(!isset($this->params["name"]) || !isset($this->body["token"])) {
                throw new \Exception("Invalid or missing parameters.");
            }
            $name = $this->params["name"];
            $token = $this->body["token"];
            $test = new TestModel();
            $test->name = $name;
            $test->token = $this->generateRandomString(5)." ## ".$token;
            $test->save();
            return $this->sendSuccess($test, "Database Successful");
        } catch (\Exception $e) {
            return $this->sendException($e);
        }
    }
    
    public function parameter($request, $res, $args) {
        try {
            if(!isset($args["name"])) {
                throw new \Exception("'name' parameter is required!");
            }
            $name = $args["name"];
            return $this->sendSuccess(null, "Hello $name!");
        } catch (\Exception $e) {
            return $this->sendException($e);
        }
    }
}

