<?php

/**
 * Created by PhpStorm.
 * User: freakyaxel
 * Date: 17/07/16
 */

namespace Api\Models;

class TestModel extends Model {

	protected $table = "test";
    protected $primaryKey = 'id';
    protected $columns = array('id','name','token');
}
