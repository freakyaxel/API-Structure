<?php

/**
 * Created by PhpStorm.
 * User: freakyaxel
 * Date: 17/07/16
 */

namespace Api\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

abstract class Model extends EloquentModel {

	public $timestamps = false;
	public $incrementing = true;
	
	public function __construct() {
		parent::__construct();
	}
}
